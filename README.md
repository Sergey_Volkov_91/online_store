Репозиторий интернет-магазина на Django 3.

Установка (для пользователей операционных систем семейства MacOs/Linux):

Открыть терминал или консоль и перейти в нужную Вам директорию

Прописать команду git clone git@bitbucket.org:Sergey_Volkov_91/online_store.git

Если Вы используете https, то: git clone https://Sergey_Volkov_91@bitbucket.org/Sergey_Volkov_91/online_store.git

Прописать следующие команды:


python3 -m venv ДиректорияВиртуальногоОкружения

source ДиректорияВиртуальногоОкружения/bin/activate

Перейти в директорию online_store

pip install -r requirements.txt

python manage.py makemigrations

python manage.py migrate

Запустить сервер - python manage.py runserver